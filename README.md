# React Examples

This repository aim to provide examples through React.js topics and each branch represents a different paradigm as their names suggests (at least it's intended to be).

Currently, there's a main branch which includes `Material UI library (@mui/material)` installed and simply added in order to demo it's usage.

Under folder react-101, there are two branches called `filterableProductTable` and `toDoApp`.

And under folder react-201, there are three branches called `globalStoreContext`, `reduxWithSlice`, and `tic-toc-toe`.

As their name suggest, they intend to do simple things. Hence as the level goes up, I found it more useful to add some more functonalities besides working mechanism, like for the branch ticTacToe. I added useMemo and useCallback functions exactly where necessary to use and sometimes I let these functions as it is for users to find out how to play with the data these functions can make use of.

Please feel free to write me from any channel for any topics from the social links from my website `erhanyasar.com.tr` if need be.

## About Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

### Available Scripts

In the project directory, you can run:

- `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

- `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

- `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

- `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can't go back!**

If you aren't satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you're on your own.

You don't have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn't feel obligated to use this feature. However we understand that this tool wouldn't be useful if you couldn't customize it when you are ready for it.

### Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
