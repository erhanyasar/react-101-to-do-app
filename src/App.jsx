import { CircularProgress, CssBaseline } from "@mui/material";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import React, { useMemo } from "react";
import Home from "./components/home";

export default function App() {

  const theme = useMemo(() => createTheme(), []);

  return (
    <React.Suspense fallback={<CircularProgress />}>
      <CssBaseline />
      <ThemeProvider theme={theme}>
        <Home />
      </ThemeProvider>
    </React.Suspense>
  );
}