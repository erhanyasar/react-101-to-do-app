import { Box, Container, Typography } from "@mui/material";

export default function Home() {
  return (
    <Container maxWidth="lg">
      <Box sx={{
        flexGrow: 1,
        textAlign: 'center'
        }}
      >
        <Typography variant="h4" component="h2">React.js Examples</Typography>
      </Box>
    </Container>
  );
}
